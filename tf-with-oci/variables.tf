variable "tenancy_ocid" {
  type        = string
  description = "REQUIRED; tenancy ocid"
}
variable "user_ocid" {
  type        = string
  description = "REQUIRED; user ocid"
}
variable "compartment_ocid" {
  type        = string
  description = "REQUIRED; OCID for compartment"
}
variable "fingerprint" {
  type        = string
  description = "REQUIRED; finger print of public key"
}
variable "private_key_path" {
  type        = string
  description = "REQUIRED; path to the private key file"
}
variable "region" {
  type        = string
  description = "Region where resources are provisioned"
  default     = "us-ashburn-1"
}
variable "display_name" {
  default     = "Sandbox_VCN"
  description = "Friendly name to VCN"
}