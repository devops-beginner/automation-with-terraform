// getting the list of AD
data "oci_identity_availability_domains" "ads" {
  compartment_id = var.tenancy_ocid
}

// printing the ads
output "ad" {
  value = data.oci_identity_availability_domains.ads.availability_domains
}