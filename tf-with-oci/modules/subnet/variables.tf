// cidr_block, compartment_id, vcn_id
variable "compartment_ocid" {
  type        = string
  description = "REQUIRED; the ocid of the target compartment within the tenancy"
}

variable "tenancy_ocid" { # required
  type        = string
  description = "REQUIRED; the ocid of our target tenancy"
}

variable "vcn_cidr" {
  type        = string
  description = "The VCN CIDR block we will cut our subnets out of"
}

variable "vcn_id" {
  type        = string
  description = "The ocid of the VCN to put the subnets into"
}