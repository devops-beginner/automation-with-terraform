// outputting the ids of subnets created
output "subnet_ocids" {
  value = {
    for name in local.ad_names : name => oci_core_subnet.subnet1[name].id
  }
}