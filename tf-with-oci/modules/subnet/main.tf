//Here, creating the subnets in each AD's, to create multiple of each resource using for.

// definig the data source to get the ADs
data "oci_identity_availability_domains" "ads" {
  compartment_id = var.tenancy_ocid
}
locals {
  ad_names = { for ad in data.oci_identity_availability_domains.ads.availability_domains : ad.name => ad.name }
}

// creating the mapping of ad and cird bloc for each.
// { ad: cidr }
locals {
  cidr_map = {
    for i in range(length(local.ad_names)) :
    values(local.ad_names)[i] => cidrsubnet(var.vcn_cidr, 12, 0+i)
  }
}

// define subnet
resource "oci_core_subnet" "subnet1" {
  for_each = local.cidr_map

  cidr_block     = each.value
  compartment_id = var.compartment_ocid
  vcn_id         = var.vcn_id

  // optional
  availability_domain = each.key
}