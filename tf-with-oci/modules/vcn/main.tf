// VCN resources
// to create a VCN, I need compartment, name for the vcn, cidr block and dns label
resource "oci_core_virtual_network" "network" {
  compartment_id = var.networking_compartment
  display_name   = "${var.sandbox_prefix}_${var.display_name}"
  cidr_block     = var.vcn_cidr_block
  dns_label      = var.vcn_dns_label
}