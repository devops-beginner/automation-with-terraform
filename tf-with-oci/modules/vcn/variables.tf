//This file has variables required for the vcn
// to create a VCN, I need compartment, name for the vcn, cidr block and dns label
variable "sandbox_prefix" {
  description = "A handy tag to prefix all our test elements with to make them easy to identify"
  default     = "training_sandbox"
}

variable "display_name" {
  type        = string
  description = "Friendly Name for our VCN"
}

variable "networking_compartment" {
  description = "Compartment ID of the networking compartment"
}

variable "vcn_cidr_block" {
  description = "CIDR Block for configuring the virtual network"
}

variable "vcn_dns_label" {
  description = "OCI DNS Label for VCN. Reference https://docs.us-phoenix-1.oraclecloud.com/Content/Network/Concepts/dns.htm"
  default     = "sndbx"
}
