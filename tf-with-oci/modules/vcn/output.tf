// this will have output of the resources and can be shred to other module as well.
output "vcn_id" {
  value = oci_core_virtual_network.network.id
}
output "vcn_cidr" {
  value = oci_core_virtual_network.network.cidr_block
}
