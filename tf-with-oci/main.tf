// Here calling the child module vcn to provision an vcn
module "tutorial_vcn" {
  source                 = "./modules/vcn"
  networking_compartment = var.compartment_ocid
  vcn_cidr_block         = "172.16.0.0/16"
  display_name           = var.display_name
}

// call subnet module to create subnets
module "subnets" {
  source           = "./modules/subnet"
  vcn_id           = module.tutorial_vcn.vcn_id
  vcn_cidr         = module.tutorial_vcn.vcn_cidr
  compartment_ocid = var.compartment_ocid
  tenancy_ocid     = var.tenancy_ocid
}