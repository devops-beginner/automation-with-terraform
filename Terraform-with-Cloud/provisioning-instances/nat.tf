/**
To setup the NAT gateway, need a static ip adrress
1. create resource fot static ip (aws eip)
2. create resource nat gateway
3. Map NAT gateway to VPC and Setup the route table
4. associate the route table to private subnets

using the NAT Gateways, subnets can connect to the internet but it will not take any public traffic
*/
// static ip
resource "aws_eip" "nat-ip" {
  vpc = true
}

// create NAT GW
resource "aws_nat_gateway" "main-ng" {
  allocation_id = aws_eip.nat-ip.id
  subnet_id     = aws_subnet.main-public-1.id
  depends_on    = [aws_internet_gateway.main-ig]
}

// Map NAT gateway to VPC and Setup the route table
resource "aws_route_table" "main-private-rt" {
  vpc_id = aws_vpc.mainvpc.id

  // push route "0.0.0.0/0" that redirects or sends the traffic to the nat gateway.
  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.main-ng.id
  }
  tags = {
    name = "main-private-rt"
  }
}

// Associate route table with private subnets (this is an option). if I strictly want want private subnet to be away from nternet then I shouldn't be doing this, rather, I just keep the private subnets just in the vpc.
resource "aws_route_table_association" "main-private-1a" {
  route_table_id = aws_route_table.main-private-rt.id
  subnet_id      = aws_subnet.main-private-1.id
}
resource "aws_route_table_association" "main-private-1b" {
  route_table_id = aws_route_table.main-private-rt.id
  subnet_id      = aws_subnet.main-private-2.id
}


