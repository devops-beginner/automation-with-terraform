//terraform file to create ec2 instances in public subnet
resource "aws_instance" "myintance-1" {
  ami           = lookup(var.AMIS, var.AWS_REGION)
  instance_type = "t2.micro"

  // give subnet
  subnet_id = aws_subnet.main-public-1.id

  // assign security group.
  vpc_security_group_ids = [aws_security_group.ssh-group.id]

  // give public ssh key
  key_name = aws_key_pair.terraform-key.key_name
}

output "public-ip" {
  value = aws_instance.myintance-1.public_ip
}