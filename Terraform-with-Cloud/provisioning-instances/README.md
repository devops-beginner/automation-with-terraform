#### Provisioning Instances on VPC

---

- This example is the an extended version of [vpc](../vpc/)
- Demonstrate to launch the instance in our own VPC insted of default vpc.
- with security groups
- using keypairs that will be uploaded by terraform.
- Now, creating instance involves following steps.
    - assign the subnet id for the resource
    - Assign security group 
    - use the SSH public key
    
- Login to instance - 

`ssh <public-ip> -l <user-name> -i <private-key>`