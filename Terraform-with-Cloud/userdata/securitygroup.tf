// creating the security group
// 1. give vpc id
// 2. set inbound  (ingress)
// 3. set outbound (egress)

resource "aws_security_group" "ssh-group" {
  vpc_id      = aws_vpc.mainvpc.id
  name        = "ssh-group"
  description = "This security group allow only ssh"

  ingress {
    from_port   = 22
    protocol    = "tcp"
    to_port     = 22
    cidr_blocks = ["0.0.0.0/0"] // allow all ssh traffic
  }
  egress {
    from_port   = 0
    protocol    = "-1"
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    name = "ssh-group"
  }
}