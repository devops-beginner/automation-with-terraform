// Creating a VPC
resource "aws_vpc" "mainvpc" {
  cidr_block           = "10.0.0.0/16"
  instance_tenancy     = "default"
  enable_dns_support   = true
  enable_dns_hostnames = true
  enable_classiclink   = false // don't link with ec2-classic

  tags = {
    name    = "mainvpc"
    default = "no"
  }
}

// createing subnets in the vpc
// note, for private sub nets, we don't map the public ip on launch

// 2 public subnets
resource "aws_subnet" "main-public-1" {
  cidr_block              = "10.0.1.0/24"
  vpc_id                  = aws_vpc.mainvpc.id
  map_public_ip_on_launch = true
  availability_zone       = "us-east-2a"

  tags = {
    name   = "main-public-1"
    public = "yes"
  }
}

// Creating an internet gateway in this VPC
resource "aws_internet_gateway" "main-ig" {
  vpc_id = aws_vpc.mainvpc.id

  tags = {
    name = "main-ig"
  }
}

// create a route tables
resource "aws_route_table" "main-public-rt" {
  vpc_id = aws_vpc.mainvpc.id
  // route all traffic to this ig.
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.main-ig.id
  }
  tags = {
    name = "main-public-rt"
  }
}

// associate this route table with public subnets

resource "aws_route_table_association" "main-public-1a" {
  route_table_id = aws_route_table.main-public-rt.id
  subnet_id      = aws_subnet.main-public-1.id
}
