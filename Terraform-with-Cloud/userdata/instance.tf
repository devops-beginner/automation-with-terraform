//terraform file to create ec2 instances in public subnet
resource "aws_instance" "myintance-1" {
  ami           = lookup(var.AMIS, var.AWS_REGION)
  instance_type = "t2.micro"

  // give subnet
  subnet_id = aws_subnet.main-public-1.id

  // assign security group.
  vpc_security_group_ids = [aws_security_group.ssh-group.id]

  // give public ssh key
  key_name = aws_key_pair.terraform-key.key_name

  // userdat through template
  user_data = data.template_cloudinit_config.cloudinit-ex.rendered
}

// creating an EBS volume
resource "aws_ebs_volume" "main-volume" {
  availability_zone = var.AWS_REGION
  size = 20
  type = "gp2"
  tags ={
    name="main-volume"
  }
}

// attach the volue to this instance
resource "aws_volume_attachment" "main-volume-attachment" {
  device_name = aws_instance.myintance-1.public_ip
  instance_id = aws_instance.myintance-1.id
  volume_id = aws_ebs_volume.main-volume.id
}

output "public-ip" {
  value = aws_instance.myintance-1.public_ip
}