#### Networking with Terraform

---
- To create a virtual private network on AWS
- Set up the private and public subnets.
- Setting up internet gateway to public subnets.
- NAT gateway to be mapped with private subnets.
