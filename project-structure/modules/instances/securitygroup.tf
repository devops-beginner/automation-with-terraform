// security group which allows only ssh
resource "aws_security_group" "ssh-group" {
  vpc_id      = var.VPC_ID
  name        = "ssh-group-${var.ENV}"
  description = "Security group which allow ssh."
  ingress {
    from_port   = 22
    protocol    = "tcp"
    to_port     = 22
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 0
    protocol    = "-1"
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    name        = "ssh-group-${var.ENV}"
    environment = var.ENV
  }
}