// private and public key
variable "PATH_TO_PUBLIC_KEY" {
  default = "keys/userkey.pub"
}
variable "PATH_TO_PRIVATE_KEY" {
  default = "keys/userkey"
}
variable "USERNAME" {
  default = "ubuntu"
}

variable "ENV" {}

variable "INSTACE_TYPE" {
  default = "t2.micro"
}

variable "PUBLIC_SUBNETS" {
  type = list
}

variable "VPC_ID" {}