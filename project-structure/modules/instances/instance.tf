// create an instance
resource "aws_instance" "myinstance" {
  ami           = data.aws_ami.ubuntu.id
  instance_type = var.INSTACE_TYPE

  // assign the subnet
  subnet_id = element(var.PUBLIC_SUBNETS, 0)

  // security group
  vpc_security_group_ids = [aws_security_group.ssh-group.id]

  //public ssh key
  key_name = aws_key_pair.my-keypair.key_name

  tags = {
    name        = "instance-${var.ENV}"
    environment = var.ENV
  }
}