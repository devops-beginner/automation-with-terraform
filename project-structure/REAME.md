#### Project Structure

---
- Aim is to separate seperate the development and production environment completely.
    - Which helps to test first in dev and then deploy on the prod.
    - Splitting up the project based on the environment helps to reduce the resource.
- Below diagram shows the overview project structure. 
![](https://gitlab.com/hegdevenky/static-files/-/raw/master/devops/terraform/terraform-project-structure.png)


