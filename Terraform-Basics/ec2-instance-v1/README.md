#### Creating AWS Resource with Terraform

---
- Open AWS account
- Create IAM user with admin access. Terraform will use this IAM user account to access the AWS resources.
- Make sure the default security group has the appropriate inbound rules set up to access the instance from your ip address. 
- Create Terraform file to spin up t2.micro instance

- Run the `terraform init` command to initialize the terraform to download all required plugins and libraries.
- Running `terraform apply` will create a specified resource on a cloud provider.
- Running the command `terraform destroy` will terminate the instance. 

> Running the `terraform destroy` in prod might remove all the resources, highly recommended not use destroy common in prod.

- `terraform plan` command looks the terraform file and show's what it's planning to do without applying the plan to infrastructure.
- The `terraform plan -out <filename>`, the changes that terraform going make (only the change for this resource) will be saved in the output file <filename>.

- So if we run the command `terraform apply <filename>`, terraform will apply the only changes. 

- Running the command `terraform apply` will run the below commands

```shell script
terraform plan -out filename # saves the changes to filename
terraform apply filename # apply the changes saved from filename
rm filename # after making the changes remove this file.
```

- However, it's good practice to explicitly run these command separately so that we can keep the changes in a file.
```shell script
terraform plan -out filename
terraform apply filename
```

- A sample terraform plan

```shell script
 ec2-instance % terraform plan
Refreshing Terraform state in-memory prior to plan...
The refreshed state will be used to calculate this plan, but will not be
persisted to local or remote state storage.


------------------------------------------------------------------------

An execution plan has been generated and is shown below.
Resource actions are indicated with the following symbols:
  + create

Terraform will perform the following actions:

  # aws_instance.my-first-instance will be created
  + resource "aws_instance" "my-first-instance" {
      + ami                          = "ami-03ffa9b61e8d2cfda"
      + arn                          = (known after apply)
      + associate_public_ip_address  = (known after apply)
      + availability_zone            = (known after apply)
      + cpu_core_count               = (known after apply)
      + cpu_threads_per_core         = (known after apply)
      + get_password_data            = false
      + host_id                      = (known after apply)
      + id                           = (known after apply)
      + instance_state               = (known after apply)
      + instance_type                = "t2.micro"
      + ipv6_address_count           = (known after apply)
      + ipv6_addresses               = (known after apply)
      + key_name                     = (known after apply)
      + network_interface_id         = (known after apply)
      + password_data                = (known after apply)
      + placement_group              = (known after apply)
      + primary_network_interface_id = (known after apply)
      + private_dns                  = (known after apply)
      + private_ip                   = (known after apply)
      + public_dns                   = (known after apply)
      + public_ip                    = (known after apply)
      + security_groups              = (known after apply)
      + source_dest_check            = true
      + subnet_id                    = (known after apply)
      + tenancy                      = (known after apply)
      + volume_tags                  = (known after apply)
      + vpc_security_group_ids       = (known after apply)

      + ebs_block_device {
          + delete_on_termination = (known after apply)
          + device_name           = (known after apply)
          + encrypted             = (known after apply)
          + iops                  = (known after apply)
          + kms_key_id            = (known after apply)
          + snapshot_id           = (known after apply)
          + volume_id             = (known after apply)
          + volume_size           = (known after apply)
          + volume_type           = (known after apply)
        }

      + ephemeral_block_device {
          + device_name  = (known after apply)
          + no_device    = (known after apply)
          + virtual_name = (known after apply)
        }

      + metadata_options {
          + http_endpoint               = (known after apply)
          + http_put_response_hop_limit = (known after apply)
          + http_tokens                 = (known after apply)
        }

      + network_interface {
          + delete_on_termination = (known after apply)
          + device_index          = (known after apply)
          + network_interface_id  = (known after apply)
        }

      + root_block_device {
          + delete_on_termination = (known after apply)
          + device_name           = (known after apply)
          + encrypted             = (known after apply)
          + iops                  = (known after apply)
          + kms_key_id            = (known after apply)
          + volume_id             = (known after apply)
          + volume_size           = (known after apply)
          + volume_type           = (known after apply)
        }
    }

Plan: 1 to add, 0 to change, 0 to destroy.

------------------------------------------------------------------------

Note: You didn't specify an "-out" parameter to save this plan, so Terraform
can't guarantee that exactly these actions will be performed if
"terraform apply" is subsequently run.
```