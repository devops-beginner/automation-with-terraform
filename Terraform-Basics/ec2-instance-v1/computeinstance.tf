// This is a terraform file to spin up an ec2 instance on AWS.
// Notice I have 2 sections provider and resource

// IMPORTANT - UPDATE ACCESS KEY AND AECRET KEY

// declaring the provider.
provider "aws" {
  access_key = "Access-key-ID"     // Your access key id from creds
  secret_key = "Secret-access-key" // your Secret-access-key from creds.
  region     = "us-east-2"
}

// resource section, resource type => aws_instance and name => my-first-instance
resource "aws_instance" "my-first-instance" {
  ami           = "ami-03ffa9b61e8d2cfda" // https://cloud-images.ubuntu.com/locator/ec2/ lookup
  instance_type = "t2.micro"
}