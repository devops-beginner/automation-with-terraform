#### Outputting the Attributes

---

- Once the resource is created, we can use the terraform `output` to display the attributes of the resources.
- We can also use the attributes to perform further actions using the resource attributes as input. 
- We can display the public information of a resource once it's provisioned.
- In this example, outputting the public ip address of provisioned instance hosting nginx server. 
- Also, storing private ip into a file.
- To run this - 

`terraform init`

`terraform apply`