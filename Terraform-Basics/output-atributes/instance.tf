// first I need to send the public key to aws, so creating aws key pair resource
resource "aws_key_pair" "userkey" {
  key_name   = "userkey"
  public_key = file(var.PATH_TO_PUBLIC_KEY)
}

// create an ec2
resource "aws_instance" "pro-instance" {
  ami           = lookup(var.AMIS, var.AWS_REGION)
  instance_type = "t2.micro"
  key_name      = aws_key_pair.userkey.key_name // give the keypair declared above

  // outputting the private ip address into a file
  provisioner "local-exec" {
    command = "echo ${aws_instance.pro-instance.private_ip} >> privateip.txt"
  }

  // once the instance is created provision the softwares

  // provisioning through file
  provisioner "file" {
    source      = "script.sh"
    destination = "/tmp/script.sh" // make sure the user has access to this location
  }

  /**
  Now execute the script.
  change add execution permisiion to this script and run as root user.
  */
  provisioner "remote-exec" {
    inline = [
      "chmod +x /tmp/script.sh",
      "sudo /tmp/script.sh"
    ]
  }

  // connect to the instance, I need private key to connect
  connection {
    host        = coalesce(self.public_ip, self.private_ip)
    type        = "ssh"
    user        = var.USERNAME
    private_key = file(var.PATH_TO_PRIVATE_KEY)
  }
}

// outputting the public ip of above instance
output "ip" {
  value = aws_instance.pro-instance.public_ip
}