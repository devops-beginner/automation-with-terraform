variable "AWS_ACCESS_KEY" {}
variable "AWS_SECRET_KEY" {}
variable "AWS_REGION" {
  default = "us-east-2"
}

// ami id's based for region.
variable "AMIS" {
  type = map(string)
  default = {
    us-east-2 = "ami-03ffa9b61e8d2cfda"
  }
}

// private and public key
variable "PATH_TO_PUBLIC_KEY" {
  default = "keys/userkey.pub"
}
variable "PATH_TO_PRIVATE_KEY" {
  default = "keys/userkey"
}
variable "USERNAME" {
  default = "ubuntu"
}