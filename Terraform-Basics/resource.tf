// Provider information such as aws, oci or azure
provider "aws" {}

// Region
variable "AWS_REGION" {
  type    = string
  default = "us-east-1"
}

variable "AMIS" {
  type = map(string)
  default = {
    us-east-1 = "my ami"
  }
}
/** resource that i need.
syntax - resource "resource_type" "name" {
  // properties
} */
resource "aws_instance" "myinstance" {
  ami           = var.AMIS[var.AWS_REGION] // map
  instance_type = "t2.micro"
}

// I can have more instances, resource names should be unique.
resource "aws_instance" "myinstance1" {
  ami           = var.AMIS[var.AWS_REGION] // map
  instance_type = "t2.small"
}

