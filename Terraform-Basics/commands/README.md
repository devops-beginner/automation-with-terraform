#### Terraform Commands

---
 - Terraform is very much focused on the resource definitions and has limited set of tools to modify, create, import these resources. 


| **Command**                  	| **Description**                                                                                                                                         	|
|--------------------------	|-----------------------------------------------------------------------------------------------------------------------------------------------------	|
| terraform apply          	| Applies the state                                                                                                                                   	|
| destroy                  	| Destroys all terraform managed state                                                                                                                	|
| fmt                      	| Rewrites terraform config files to a canonical format<br>and style. Usually use the command before committing <br>to make sure the formatting.      	|
| get                      	| Download and Update modules                                                                                                                         	|
| graph                    	| Gives visual representation of configs & plan                                                                                                       	|
| import [options] ADDR ID 	| Finds the infrastructure resource identified with ID & import<br>the state into terraform.tfstate with resource if address.<br>  We can import an existing infrastructure.                      	|
| output [options] [NAME]  	| Outputs any of the resource. Using NAME will only output a <br>specific resource.                                                                   	|
| refresh                  	| This command can identify the differences between terraform<br>state file and a remote infrastructure state.It refresh<br>the remote state.         	|
| remote                   	| Configure remote state storage.                                                                                                                     	|
| show                     	| Shows human readable output from a state or a plan.                                                                                                 	|
| state                    	| This command can be used for advanced state management.<br>For ex- copying a resource to another, changing the name or<br>any state.                	|
| taint                    	| This command can be used to manually mark a resource as tainted.<br>Which means, the resource will be destroyed and recreated at the<br>next apply. 	|
| validate                 	| Validates terraform syntax of a terraform file.                                                                                                     	|
| untaint                  	| Undo a taint.                                                                                                                                       	|