resource "aws_instance" "myinstance" {
  ami           = lookup(var.AMIS, var.AWS_REGION) // lookup is a function to get the value for a given key from a map.
  instance_type = "t2.micro"
}