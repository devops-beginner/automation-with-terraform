#### Creating AWS Resource with Terraform

---
This example is the improvised version of previous version [here](../ec2-instance-v1/)

- It does the same thing as v1, but the terraform file is modularized further into provider, instance and variables.
- Note, the access key and secret keys are in `terraform.tfvars` which shouldn't be checked into version control.


