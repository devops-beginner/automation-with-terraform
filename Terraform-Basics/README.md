## Terraform Basics

- `terrform version` is the command to know the version of terrform installed.
- `terraform console` We can use the console to run terraform files i.e .tf files. These variables defined in files such as hello-world.tf are available on console.

#### Variables
- **Creating a variable**
```hcl-terraform
//Creating a variable
variable "hellovar" {
type = string
  default = "Hello World from Terraform!"
}
```
- `var.hellovar` will print the message.
```hcl-terraform
> var.hellovar
Hello World from Terraform!

// Another way of calling the variable.
> "${var.hellovar}"
Hello World from Terraform!
```
- Variables can also be used hide the secrets, by creating a file with variables which will not be pushed to the version control.
- Use the variables for elements that change, for example AMI which might change, so it's good practice having a variable to hold the ami id.
- Refer [this](ec2-instance-v2/) example

- **Creating a map**
```hcl-terraform
// Map
variable "myMap" {
  type = map(string)
  default = {
    key1 = "Value 1"
  }
}
```
```shell script
Terraform-Basics % terraform console
> var.myMap
{
  "key1" = "Value 1"
}
> var.myMap["key1"]
Value 1
> "${var.myMap["key1"]}"
Value 1
>  
```
- **Creating a list**
```hcl-terraform
// list
variable "myList" {
  type = list
  default = [1,2,3,4,5]
}
```
```shell script
Terraform-Basics % terraform console
> var.myList
[
  1,
  2,
  3,
  4,
  5,
]
> var.myList[0]
1
```
- While creating a list or map, if we don't specify the type, it will infer the type.
- `terraform init` - download and install the plugins and libraries.

#### Terraform Variable Types
- keyword `variable` is used for declaring variable.
- Type in a variable is optional. 
- Terraform simple variable types 
    - String / string
    - Number / number
    - Bool / bool - Boolean
- Terraform's Complex types
    - List(type)
    - Set(type)
    - Map(type)
    - Tuple(`[<type>, ...]`)
    - Object(`{<atribute_name>=<type>}`)
- Object in terraform is like a map, but each element can have different types.
- A tuple is like a list, but each element can have a different type.
 ```hcl-terraform
variable "myTuple"{
type = tuple
default = ["Hello World", "A sentance", 18, true]
}
```
