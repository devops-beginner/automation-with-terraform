#### Data Sources
- For certain providers, terraform provides datasources. 
- We can get dynamic information from the datasource such as ip address, list of availability zones, amis etc.
- We can use the data source for wide range of operations.
- In this example, first get the **ip ranges** for ec2 service for europe region.
- Then create a security group using the information from above data. 

`terraform init`

`terraform plan`

`terraform apply`

- Running above commands will create a new security group. 