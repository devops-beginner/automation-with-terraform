// pulling the data of all ip addresses of ec2 service from region eu-west-1 and eu-central-1
data "aws_ip_ranges" "european_ec2" {
  services = ["ec2"]
  regions  = ["eu-west-1"]
}

/**
create a security groups from the above data.
resource /service - aws_security_group
name - europe_group
*/
resource "aws_security_group" "europe_group" {
  name = "europe_group"
  // setting inboud rules for the security group
  ingress {
    from_port = "443"
    protocol  = "tcp"
    to_port   = "443"
    // taking the cidr blocks from above data.
    cidr_blocks = data.aws_ip_ranges.european_ec2.cidr_blocks
  }
  // creating the tag
  tags = {
    DateCreated = data.aws_ip_ranges.european_ec2.create_date
    SyncToken   = data.aws_ip_ranges.european_ec2.sync_token
  }
}