// backend detail. S3 bucket name and a tag
terraform {
  backend "s3" {
    bucket = "my-terra-state" // name of the bucket
    key    = "terraform/remotestate"
    region = "us-east-2"
  }
}