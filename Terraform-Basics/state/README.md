#### Remote States

---
- Terraform store the state of the infrastructure in a `terraform.tfstate` file.
- Usually the backup of this file is stored locally `terraform.tfstate.backup`
- When we run the `apply` command, terraform update the state files.
- We can store the state files to a remote location using the **backend** functionality. (Default is **local backend**, which is local state file.)
- Some other backends are
    - **terraform enterprise** which is a commercial solution
    - **consul**
    - S3

- Using the backend improves **collaboration**, avoid sensitive information to be stored locally.
- There are 2 steps to configure a remote state
    - add backed code to .tf file
    - run the initialization process.
- We can also use read-only remote stores such as datasource. 

- This example shows, how to use s3 backend.

Steps - 
- Create a S3 bucket with versioning enabled.
- configure AWS Access Key and Secret using `aws configure`
- These credentials will be saved in `~/.aws/credentials`
- Create a backed.tf file which has backed configuration
- Run `terraform init` to initialize, which will update the backend
```shell script
 state % terraform init

Initializing the backend...

Successfully configured the backend "s3"! Terraform will automatically
use this backend unless the backend configuration changes.

Initializing provider plugins...
- Checking for available provider plugins...
- Downloading plugin for provider "aws" (hashicorp/aws) 2.58.0...

The following providers do not have any version constraints in configuration,
so the latest version was installed.

To prevent automatic upgrades to new major versions that may contain breaking
changes, it is recommended to add version = "..." constraints to the
corresponding provider blocks in configuration, with the constraint strings
suggested below.

* provider.aws: version = "~> 2.58"

Terraform has been successfully initialized!

You may now begin working with Terraform. Try running "terraform plan" to see
any changes that are required for your infrastructure. All Terraform commands
should now work.

If you ever set or change modules or backend configuration for Terraform,
rerun this command to reinitialize your working directory. If you forget, other
commands will detect it and remind you to do so if necessary.
```

- Command to list the resources in state file is `list state file resources`
