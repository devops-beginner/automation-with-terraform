#### Software Provisioning

- Scripts can be upload to install the software.
- While provisioning on AWS, normally SSH key pair will be used.
- Once the script uploaded we can execute using remote-exec.

![](https://gitlab.com/hegdevenky/static-files/-/raw/master/devops/terraform/sf-provisioning.jpg)

- This example will create an ec2 instance on AWS and install nginx upon running `terraform apply`.
- Hit the public ip of the ec2 instance should bring up nginx welcome page.