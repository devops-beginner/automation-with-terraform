//Creating a variable
variable "hellovar" {
  type    = string
  default = "Hello World from Terraform!"
}

// Map
variable "myMap" {
  type = map(string)
  default = {
    key1 = "Value 1"
  }
}

// list
variable "myList" {
  type    = list
  default = [1, 2, 3, 4, 5]
}

// functions
