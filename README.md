## Automation-with-Terraform

- [Terraform Basics](Terraform-Basics/)
- [Provisioning the Software](Terraform-Basics/software-provisioning/)
- [Outputting Attributes](Terraform-Basics/output-atributes/)
- [State](Terraform-Basics/state/)
- [Data Source](Terraform-Basics/data-source/)
- [Commands](Terraform-Basics/commands/)
- [Creating Virtual Private Network with AWS](/Terraform-with-Cloud/vpc/)
- [Provisioning Instances in Private Newtwork](/Terraform-with-Cloud/provisioning-instances/)
- [Project Structure](/project-structure/)

