
- Some of the terraform components are 
    - Variables
    - Resources
    - Data Source
    
- We can get the path information using - `path.TYPE`
    - path.cwd -> current directory
    - path.module -> module path
    - path.root -> root module path

**Conditionals** are if else statements inside an Interpolation. Syntax - `condition ? trueval : falseval`

ex - 
```hcl-terraform
// if the env is prod then have 3 instances otherwise 1
resource "aws_instance" "example"{
count = "${var.env == "prod" ? 3:1}"
}
```

##### Builtin Functions

- Builtin functions reference [here](https://www.terraform.io/docs/configuration/functions.html)

##### For and For each loop

![](https://gitlab.com/hegdevenky/static-files/-/raw/master/devops/terraform/for-each.jpg)

